<?php
/**
 * Примеры работы с файлами.
 */
// Откроем файл в режиме для записи
$fp = fopen("./data.txt", "r+");

// Запишем в файл данные
fwrite($fp, "_TEST_");

//Прочитаем данный из файла
$data = fread($fp, filesize("data.txt"));
echo $data;


// Закроем файл
fclose($fp);
